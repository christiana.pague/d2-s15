//alert('Hello!');


/* Operators

	- Arithmetic Operators
	- Assignment Operator
	- Compound Assignment Operators
	- Comparison Operators
	- Logical Operators
*/

/* Arithmetic Operators
		- perform arithmetic operations on numerical values(or variables) as their operands and returns a single numerical value

	Operators			Description
		+				Addition
		-				Subtraction
		*				Multiplication
		/				Division
		%				Modulus (remainder)
		++				Increment
		--				Decrement

	Operand - the numbers/variables in an expression
	Operator - operation to be done in the operands

*/

// Examples: 
	//	let x = 20, y = 10, z = 100, w = 3;
	// console.log(x + y);		// 10

	//console.log('x' + 'y');	// concatenation

	// console.log(x - y);

	// console.log(x * y);

	// console.log(x / y);

	// console.log(z % a);

	//**show in console let x = 20, y = 10;
	// console.log(++x);
	// console.log(--y);

	//**show in console let a = 20, b = 10;
	//console.log(a++);  
	//console.log(b--);
			//if the variable comes first before the increment or decrement operator, the actual value returns first before executing it's arithmetic operator.


	// Arithmetic Operations on two numbers:

	let sumOfTwoNumbers = 65 + 38;
	//console.log(sumOfTwoNumbers);

	// Arithmetic Operation on variables:
	let b = 25;
	let c = 30;

	let totalSum = b + c;
	//console.log(totalSum);

	// Arithmetic Operation on expressions:
		// expression is a combination of literals, variables, operators that evaluates to a single value.
			// **not show this unless asked**
				//Conceptually, there are two types of expressions: those that assign a value to a variable, and those that simply have a value.

	let d = 4;
	let totalExpression = (52 - 29) * d;
	//console.log(totalExpression);

/*skip Assignment Operator because it was discussed yesterday*/
/* Assignment Operator (=)
	- assigns the value to a variable

	//Example:

	let e = 14;
	f = e;
	console.log(f);

*/
/* Compound Assignment Operator
		- shorthand for assignment and arithmetic operation
		- performing arithmetic operation while assigning the return value (result) back to the variable.

	Operators	Example/Name 						Same as:
		=		x = y 	assignment					x = y
		+=		x += y 	addtion assignment			x = x + y
		-= 		x -= y 	subtraction assignment		x = x - y
		*=		x *= y 	multiplication assignment	x = x * y
		/= 		x /= y 	division assignment			x = x / y
		%= 		x %= y 	modulus assignment			x = x % y	
*/
		// let x = 15
		//console.log(x = 3);  // 3

		// console.log(x += 8);  // 23

		//console.log(x -= 4);	// 11

		//console.log(x *= 12); // 180

		//console.log(x /= 3);	// 5

		//console.log(x %= 2);	// 1
	
/* Mini-activity

	using console.log(), perform "Compound Assignment Operations" using the declared variable below:

	let k = 35;
	y value will be user defined.

	// console.log(k += 5);  // 40
				   k + 5 		// perform arithmetic operation
				   k = result	// result assigned back to variable

	// console.log(k -= 5);    // 30

	// console.log(k *= 5);    // 175

	// console.log(k /= 5);   // 7

	// console.log(k %= 5); 	// 0

*/

/* Comparison Operators	(expecting true or false return)
		- compares its operands and returns a logical value (true or false) based on whether the comparison is true or false.

		Operators 			Description
			==				Equality
			!=				Not equal/ Inequality
			===				Strict Equality
			!==				Strict Inequality
			<				Less than
			<=				Less than or equal to
			>				Greater than
			>=				Greater than or equal to
*/

	//Equality Operator (==)
		// - compares the "sameness" of values regardless of their data types

		//console.log('Jan' == 'Jan');  // true
		//console.log(true == true);	// true
		//console.log(false == false); //true
		//console.log(true == false);	// false
		//console.log(false == true);	// false

	//Inequality Operator (!=)

		//console.log('Denmark' != 'American');	//true
		//console.log('Jeans' != 'Jeans');		//false
		//console.log(3 != 3.00)	//false


	// Strict Equality Operator (===)
			//- compares the "sameness of values" AND "data types" 
				// which means it disrequards the "Type Coercion" being done by Javascript behind the scene.
		
		//console.log(1 === 1); // same value & data type



	// Strict Inequality Operator (!==)

		//console.log(1 !== 1);	//false bec they are same values & data type


// What is the difference between Equality (==) and Strict Equality Operator (===)?
	// -Type Coercion is being ignored in strict equality operator

	//console.log(1 == '1');  // true  -- comparing same value regardless of data type

	//console.log(1 === '1');	// false -- comparing both value and data type

//***what really happens behind the scene?

	//console.log(1 === parseInt('1'))

	//what's really the value of this parseInt('1')?
		//console.log(parseInt('1'));	// 1 - numerical value


/* Type Coercion
	source = https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Equality
	- automatic or implilcit conversion of values from one data type to another (such as string to a number)

		parseInt() - for whole numbers (no decimal numbers included)

		parseFloat() - for decimal numbers
			- this will look at the first decimal and throw the other decimals that comes after it.
			
			let numbers = "123.456.789.101"
			console.log(parseFloat(numbers));	// 123.456

			let chocolate = "50.99 per box"
			console.log(parseFloat(chocolate));	// 50.99

		toFixed() - to set number of decimal places

			let coercion = 12.12 + 12; // 24.119999999999997
			console.log(coercion.toFixed(4)) // 24.1200

	** when does type coercion implicitly being applied behind the scene? (implicit means implied though not plainly expressed.)
	- when COMPARING two diferent data types.
	*/

	// Another example of type coercion:

	let isStudent = true; // converted to 1
	let isEnrolled = "true";
	// let isEnrolled = parseInt("true"); // NaN = Not a Number
	// console.log(isEnrolled);

	let isStatus = isStudent == isEnrolled;
	//console.log(isStatus);		// false

	// after conversion, it would look like this:
	//1 == NaN // false

	// let isStatus = isStudent === isEnrolled;
	// console.log(isStatus); //false






/* mini activity

	1. Perform Arithmetic operation on two operands. One numurical value and the other is a variable that holds numerical value.
	Display the result in the console.
	
	Solution:
		let a = 35;

		let product = 17 * a;
		console.log(product);

	
	2. Perform arithmetic operation on two operands and assign the result back to a variable.
	Display the result in the console.
		
	Solution:

		let x = 50;

		console.log(x %= 5);	// 0


	3. Compare two operands with the same value but different data types and display the answer in the console (using both equality & strict equality operator to compare how type coercion would affect it's result)

	Solution:

	let str = 1;
	let num = "1";

	let isResult = str == num;   // true
	let isResult = str === num;  // false
	console.log(isResult);   	


*/




/* Logical Operator
	
	Operator 			Description
	  &&				  AND
	  || 				  OR
	  !					  NOT - negates the value

Scenarios:



	// If we use && AND ampersand(all operands should be true or false alone);

	console.log(true && true);		// true
	console.log(true && false);		// false
	console.log(false && false);	// false

	// If we use || OR double pipe(atleast 1 of the 2 operands / or some operands are either true or false)

	console.log(true || true);		// true
	console.log(false || true);		// true
	console.log(true || false);		// true
	console.log(false || false);	// false

	// If we use ! NOT logical operator, it negates the value

	console.log(!true);
	console.log(!false);
	
	______________

	let isAdmin = true;	 // false = 0
	let isInstructor = '1';

	let isAnswer = !isAdmin == isInstructor;
	console.log(isAnswer);
	
	____________
	
	Q: what will be the result of console.log()?

	let x = 3;
	let y = 4;
	console.log( x > 7 || 8 < y);	// false

	____________
	
	Q: What is the value of chatSiBoss?

	let isOnline = true;
	let isBoss = false;
	let chatSiBoss = isOnline && isBoss;
	console.log(chatSiBoss)		// false
	
	_____________

	Q: What is the value of isHigher?

	let faveNum = 333;
	let choosenNum = 245;
	let isHigher = !(choosenNum > faveNum || faveNum >= choosenNum)
	console.log(isHigher);	// false
	
	______________
	
	Q: What is the value of isTralse?

	let isTrue = true;
	let isFalse = false;

	let isTralse = !( !isFalse != !isFalse && isFalse !== isTrue || (!isTrue != !isFalse && isFalse != !isTrue))
	console.log(isTralse);	//  true

	!(false && true || true && false) //compare two operands w/ampersand
	!(false || false)	//only one condition should be true w/ OR || 
	!(false)			// false is negated by NOT Operator
	= true 				// final answer is true
	

*/

/* Control Structures
	
	What is a control structure?
		- sorts outs whether the given set of instructions/statements are executed based on the condition whether is it true.

		// if else statement
		// switch statement
		// try catch finally



/*
	If else statement

		Syntax:

		if (condition){		// if condition is true, statement 1 will be executed
	
			statement 1

		} else {			// if condition is false, statement 2 will be executed
	
			statement 2

		}

*/

// Example of if statement
	// if statement can stand alone even without the else statement

/*
	let age = 20;

	if (age <= 18){

		// console.log('Not allowed to enter');
		alert('Not allowed to enter');

	} 

*/
/*

// adding else statement
	// this will make the whole statement more sense if the first condition is not true

	//let age = 20; 	// or use prompt();
	let age = prompt('Enter your age');

	if (age <= 18){

		// console.log('Not allowed to enter');
		alert('Not allowed to enter');

	} else {

		alert('Allowed to enter');
	}
*/

/* Mini Activity 

	Create a conditional statement that if height is below 150, display "Did not pass the minimum height requirement". If above 150, display "Passed the minimum height requirement".

	**Stretch goal: Put it inside a function

	Solution:


	// < 150 = "Did not pass the minimum height requirement"
	// > 150 = "Passed the minimum height requirement"

	let height = prompt("Enter your height");

	if ( height < 150 ){

			alert("Did not pass the minimum height requirement");

	} else {

		alert("Passed the minimum height requirement");

	}


	//**Stretch goal: Put it inside a function


	function measureHeight(height){


		if ( height < 150 ){

			alert("Did not pass the minimum height requirement");

		} else {

			alert("Passed the minimum height requirement");

		}
		
	}

	let myHeight = prompt("Enter your height");

	measureHeight(myHeight);

*/

/* mini activity

	Create a function that accepts a name, and if the name equals to Romeo, display "Juliet", if the name is not equal to Romeo, display "Hamlet"
	
	// create a function
	// accepts a name --- parameter
	// condtion (2 possible outcomes):
		- if name === romeo ---- display "Juliet"
		- if name !== romeo ---- display "Hamlet"

	Solution:



	function enterName(name){

		if( name === "romeo" ){

			console.log("Juliet");

		} else {

			console.log("Hamlet")
		}

	}

	enterName('joy');

*/


// if... else if ... else
	// - just like if else statement, if...else if... else are expecting multiple possible outcomes that only one of the conditions may be true.


	let price = 100;

	if(price < 50){

		//console.log('affordable');

	} else if (price < 75) {

		//console.log('pricey');

	} else if (price < 85){

		//console.log('not affordable');

	} else {

		//console.log('out of budge. Tara window shopping!');
	}


/* mini activity 

	Put this inside a function which accepts price and display 4 possible outcomes.

	function enterPrice(price){

		if(price < 50){

			console.log("affordable");

		} else if(price < 75){

			console.log("pricey")

		} else if(price < 80){

			console.log("not affordable")

		} else {

			console.log("out of budget. Tara window shopping!")
		}
	}
	enterPrice(10);

*/

/* mini activity (application from our discussion if...else if... else statement)

Determine the Typhoon Intensity with the following data:

1. Windspeed of 30, not a typhoon yet
2. Windspeed of 61, a tropical depression is detected
3. Windspeed of 62-88, a tropical storm is detected
4. Windspeed of 89-117, a severe tropical storm is detected

**stretch goal: Put it inside a function



// Solution 1:

		let windSpeed = 50;
	// let windSpeed = prompt("Enter Windspeed");

		if (windSpeed <= 30){

			alert('Not a typhoon');

		} else if (windSpeed >= 31 && windSpeed <= 61) {

			alert('Tropical Depression Detected');

		} else if ( windSpeed >= 62 && windSpeed <= 88){

			alert('Tropical Storm Detected');

		} else if ( windSpeed >= 89 && windSpeed <= 117){

			alert('Sever Tropical Storm Detected');

		} else {

			alert('Typhoon Detected');

		}

// Solution 2: Using a function
*/
	
	function determineTyphoonIntensity(windSpeed){


		if (windSpeed <= 30){

			alert('Not a typhoon');

		} else if (windSpeed <= 61) {

			alert('Tropical Depression Detected');

		} else if ( windSpeed >= 62 && windSpeed <= 88){

			alert('Tropical Storm Detected');

		} else if ( windSpeed >= 89 && windSpeed <= 117){

			alert('Sever Tropical Storm Detected');

		} else {

			alert('Typhoon Detected');

		}
	}
	

	//determineTyphoonIntensity(120);			// supplying argument in the invocation
	// let number = prompt("Enter Windspeed");
	// determineTyphoonIntensity(number);  


/*
	.length
		- the length property returns the length of a string (number of characters) or total number of elements inside of an array
		- the length of an empty string is 0

	Example:
*/

	let name = 'Joy Pague'

	//console.log(name.length);

	let arr1 = [1, 54, 54, 52, 657, 354, 55, 7, 6];

	//console.log(arr1.length);

	// Example:

	function getName(fName, lName){

		if(fName.length !== 0 && lName.length !==0){

			//console.log('Thanks for logging your full name');

		} else {

			//console.log('Please fill in your complete name');
		}
	}

	//getName('Mark', 'Vivar');
	//getName('','');

/* 
	Create a function that will accept a number, if the number given by the user is odd number, display "odd number", if even number, display "even number"

	Before logging odd or even number, check first the input of a user. If the user input is a string such as "one", "two", "three", display "unexpected input" instead of "odd/even number".



	function oddOrEven(num){

		if (num % 2 !== 0){

			console.log("odd number");

		} else {

			console.log("even number");
		}
	}
	oddOrEven('10');
	//oddOrEven(5);

	// what if the user input is a word form of a number?
	//oddOrEven('fourteen');  // -- a bug

	// what if the user input is a string form of a number?
	//oddOrEven('3');


	//Before logging odd or even number, check first the input of a user. If the user input is a string such as "one", "two", "three", display "unexpected input" instead of "odd/even number".

	function oddOrEven2(num){

		if(typeof parseInt(num) !== "number"){

			console.log("unexpected input")

		} else {

			if (num % 2 !== 0){

			console.log("odd number");

			} else {

				console.log("even number");
			}
		}
	}
	oddOrEven2('twenty');
	oddOrEven2('2');


	function oddOrEven3(num){

		if(isNaN(parseInt(num)) || typeof parseInt(num) !== "number"){
			// isNaN() function determines whether a value is an illegal number (Not-a-Number). This function returns true if the value equates to NaN

			console.log("unexpected input")

		} else {

			if (num % 2 !== 0){

			console.log("odd number");

			} else {

				console.log("even number");
			}
		}
	}
	oddOrEven3('fifteen');
	oddOrEven3('18');
	oddOrEven3(5);
	oddOrEven3('computer');

*/

/*
	Switch Statement
		- just like if else if else, switch cases provides multiple possible outcomes that one of the cases given may be true.
		- switch only accepts single condition


		Switch Syntax:

		switch(condition){
			
			case: 
					break;
			default:
		}
*/

	switch(8){
		case 1: console.log('1');
			break;
		case 2: console.log('2');
			break;
		case 3: console.log('3');
			break;
		case 4: console.log('4');
			break;
		default: console.log('not found');
	}


	let userRole = 'admin';

	switch(userRole){
		case 'admin': console.log('has authorization');
			break;
		case 'user': console.log('ask for authorization');
			break;
		default: console.log('no access');
	}
	
	function color(){

		let text = document.getElementById('input').value

		switch(text){
		case 'red': document.getElementById('hello').style.color = 'red';
			break;
		case 'blue': document.getElementById('hello').style.color = 'blue';
			break;
		case 'green': document.getElementById('hello').style.color = 'green';
			break;
		default: 
		}
	}

/* 
	Using switch statement, display whether the number is "even number" or "odd number" when the condition is a number divisible by 2.



	switch('twenty' % 2 ) {
	case 0:
		console.log("even number")
		break;
	case 1:
		console.log("odd number");
		break;
	default :
		console.log("unexpected input");
}	

*/

/* Ternary Operator
	- shorthand for if else

	Operator 			Description
		?					if
		: 					else

	let myAge = 50;

	myAge > 18 ? console.log('above 18') : console.log('below 18');

	myAge < 18 ? console.log('above 18') : console.log('below 18');
*/

/* Try Catch (Try Catch Finally)
	// error handling
	// finally will run no matter what happens, even if there's an error that has been catched.

	syntax:

	try {

		statement 1

	} catch(){
		
		statement 2
	}
*/
	
	let myNumber = prompt('number please');

	try {

		if(myNumber < 4){

			console.log('true');

		} else {

			throw 'error message, not true'
		}

	} catch(err){

		console.error(err);
	}


	function showIntensityAlert(windSpeed){

		try{

			alert(determineTyphoonIntensity(windSpeed))

		} catch(err){

			alert(err.message);
			console.warn(err.message);

		} finally {

			alert('Intensity updates will show new alert.')
		}
	}

	showIntensityAlert(prompt('enter a number'));


/*

Graded Activity

	Activity

	Create a function getInfo that will accept the following details:
		name
		email
		password
		confirm password

	The created function will display "Thanks for logging your information" only if the conditions true:
		1. all the information given are not empty
		2. password is ATLEAST 8 characters
		3. confirm password and password must be equal/matched

	If the function's supplied arguments are empty/incorrect, display "Error: Please check your information".

Solution:

*/

	function getInfo(name, email, pw, cpw){

		if (name.length !== 0 &&
			email.length !== 0 &&
			pw.length !== 0 &&
			cpw.length !== 0 &&
			pw.length >= 8 &&
			pw === cpw){

			console.log("Thanks for logging your information");

		} else {

			console.log("Error: Please check your information");
			
		}
	}

	
	








	